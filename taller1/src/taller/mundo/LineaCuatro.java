package taller.mundo;

import java.util.ArrayList;
import java.util.Random;
public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;
	
	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;
	
	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;
	
	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;
	
	/**
	 * El número del jugador en turno
	 */
	private int numeroColumnas;
	private int turno;
	private int numeroParaGanar;
	
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol, int numer)
	{
		 numeroColumnas=pCol;
		tablero = new String[pFil][pCol];
		numeroParaGanar=numer;
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[i].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}
	
	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}
	
	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		Random  rnd = new Random();
		boolean sepudo=false;
		int i=(int) (rnd.nextDouble()*numeroColumnas);
		boolean sepudo2=false;
		for(int z=0;z<tablero.length&&sepudo2==false;z++)
		{
			for(int w=0;w<tablero[z].length&&sepudo2==false;w++)
			{
				if(tablero[z][w].equals("___"))
				{
					sepudo2=true;
				}
			}
		}
		if(sepudo2==false)
		{
			sepudo=true;
		}
		while (sepudo==false)
		{
			for(int j=tablero.length;j>0;j--)
			{
				if(tablero[j][i].equals("___"))
				{
					String simbolo=null;
					sepudo=true;
					terminar(j,i);
				}
			}
			i=(int) (rnd.nextDouble()*numeroColumnas);
		}
		//TODO
	}
	
	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		boolean sepudo=false;
		for(int i=tablero.length;i>0;i--)
		{
			if(tablero[i][col].equals("___"))
			{
				String simbolo=null;
				sepudo=true;
				for(int j=0;j<jugadores.size();j++)
				{
					if(jugadores.get(j).darNombre().equals(atacante))
					{
						simbolo=jugadores.get(j).darSimbolo();
					}
				}
				tablero[i][col]=simbolo;
				terminar(i,col);
			}
			}
		turno=turno++;
		if(turno>jugadores.size())
		{
			turno=0;
		}
		atacante=jugadores.get(turno).darNombre();
		//TODO
		return sepudo;
	}
	
	/**
	 * Determina si una jugada causa el fin del juego
	 */
	public void terminar(int fil,int col)
	{
		boolean sepudo2=false;
		//TODO
		if(sepudo2==false)
			{
			int total=0;
			for (int i=0;i<jugadores.size();i++)
			{
				int u=0;
				for(int j=0;j<tablero[fil].length&&sepudo2==false;j++)
			{
				if(jugadores.get(i).darSimbolo().equals(tablero[fil][j]))
				{
					u++;
					if(u>total)
					{
						u=total;
					}
				}
			}
		}
			if(total==numeroParaGanar)
			{
				sepudo2=true;
				finJuego=true;
			}
		}
		if(sepudo2==false)
		{
			int total=0;
			for(int i=0;i<jugadores.size();i++)
		{
				int u=0;
			for(int j=0;j<tablero.length&&sepudo2==false;j++)
			{
				if(jugadores.get(i).darSimbolo().equals(tablero[j][col]))
				{
					u++;
					if(u>total)
					{
						u=total;
					}
				}
			}
		}
			if(total==numeroParaGanar)
			{
				sepudo2=true;
				finJuego=true;
			}
		}
		if(sepudo2==false)
			if(sepudo2==false)
			{
				int total=0;
				for(int i=0;i<jugadores.size();i++)
			{
					int u=0;
					for(int s=fil;s<tablero.length&&sepudo2==false;s++)
		{
			for(int j=col;j<tablero[s].length&&sepudo2==false;j++)
			{
				if(jugadores.get(i).darSimbolo().equals(tablero[s][j]))
				{
					u++;
					if(u>total)
					{
						u=total;
					}
			}
		}
			}
				for(int s=fil;s>=0&&sepudo2==false;s--)
				{
					for(int j=col;j>=0&&sepudo2==false;j--)
					{
						if(jugadores.get(i).darSimbolo().equals(tablero[s][j]))
						{
							u++;
							if(u>total)
							{
								u=total;
							}
					}
			
					}
					if(total==numeroParaGanar)
					{
						sepudo2=true;
						finJuego=true;
					}
			}
		if(sepudo2==false)
		{for(int z=0;z<tablero.length&&sepudo2==false;z++)
		{
			for(int w=0;w<tablero[z].length&&sepudo2==false;w++)
			{
				if(tablero[z][w].equals("___"))
				{
					sepudo2=true;
				}
			}
	   }
		if(sepudo2==false)
		{
			finJuego=true;
	}}

	}

			}}}
