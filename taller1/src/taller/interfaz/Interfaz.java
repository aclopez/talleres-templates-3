package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;
	
	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;
	
	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
    }
	
	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		sc= new Scanner (System.in);
		System.out.println("nombre jugador");
		System.out.println("simbolo");
		System.out.println("partidas necesarias para ganar");
		System.out.println("por favor separar con ; y repita el proceso por cada jugador");
		String d=null;
		String s=null;
		int v=0;
		ArrayList t=new ArrayList();
		while(true)
		{
			String opt=sc.next();
			String[] h=opt.split(";");
			for(int y=0;y<h.length;y=y+3)
			{d=h[y];
			s=h[y+1];
			v=Integer.parseInt(h[y+2]);
			Jugador tx=new Jugador(d,s);
			t.add(tx);
			}
			juego(t,v);
		}
       //TODO
	}
	
	/**
	 * Modera el juego entre jugadores
	 * @param v 
	 */
	public void juego(ArrayList tp, int v)
	{
		int filas=0;
		int columnas=0;
			while(true)
			{
			  System.out.println("tamaño filas igual o mayor de 4");
			  System.out.println("tamaño columnas igual o mayor de 4");
			  String opt=sc.next();
			  String[] h=opt.split(";");
			  filas=Integer.parseInt(h[0]);
			  columnas=Integer.parseInt(h[1]);
			if(filas>=4&&columnas>=4)
			{
				juego=new LineaCuatro(tp,filas,columnas,v);
				imprimirTablero();
				while (juego.fin()==false)
				{
					System.out.println("EL atacante es: "+juego.darAtacante());
					System.out.println("numero de columna para atacar ");
					int colu=Integer.parseInt(sc.next());
					boolean jugada=juego.registrarJugada(colu);
					while (jugada==false)
					{
						System.out.println("no se pudo atacar, elija una nueva columna para atacar");
						colu=Integer.parseInt(sc.next());
						jugada=juego.registrarJugada(colu);
					}
						System.out.println("se realizo la jugada ");
					    imprimirTablero();
				}
				if(juego.fin()==true)
				{
					System.out.println("fin del juego, el ganador es: "+juego.darAtacante());
					System.out.println("gracias por jugar");
					System.out.println("¿quiere jugar una nueva partida?");
					System.out.println("1. si");
					System.out.println("2. no");
					int s=Integer.parseInt(sc.next());
					if(s==1)
					{
						empezarJuego();
					}
				}
			}
		}
		}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		sc= new Scanner (System.in);
		System.out.println("nombre jugador 1");
		System.out.println("simbolo");
		System.out.println("partidas necesarias para ganar separe por ;");
		String d=null;
		String s=null;
		int tx=0;
		while(true)
		{
			String opt=sc.next();
			String[] h=opt.split(";");
			d=h[0];
			s=h[1];
			tx=Integer.parseInt(h[2]);
		Jugador v=new Jugador(d,s);
		juegoMaquina(v,tx);
		}
		//TODO
	}
	/**
	 * Modera el juego contra la máquina
	 * @param v 
	 * @param tx 
	 */
	public void juegoMaquina(Jugador v, int tx)
	{
		int w=0;
		int filas=0;
		int columnas=0;
		Jugador maquina=new Jugador("maquina","s");
		ArrayList pt=new ArrayList ();
		pt.add(maquina);
		pt.add(v);
		sc= new Scanner (System.in);
		 System.out.println("tamaño filas");
		  System.out.println("tamaño columnas");
		  while (true)
		{
			  String opt=sc.next();
				String[] h=opt.split(";");
				filas=Integer.parseInt(h[0]);
				columnas=Integer.parseInt(h[1]);	  
		if(filas>=4&&columnas>=4)
		{
			juego=new LineaCuatro(pt,filas,columnas,tx);
			imprimirTablero();
			while (juego.fin()==false)
			{
				System.out.println("EL atacante es: "+juego.darAtacante());
				if (juego.darAtacante().equals(maquina.darNombre()))
				{
					juego.registrarJugadaAleatoria();
				}
				else
				{
				System.out.println("numero de columna para atacar ");
				int colu=Integer.parseInt(sc.next());
				boolean jugada=juego.registrarJugada(colu);
				while (jugada==false)
				{
					System.out.println("no se pudo atacar, elija una nueva columna para atacar");
					colu=Integer.parseInt(sc.next());
					jugada=juego.registrarJugada(colu);
				}
					System.out.println("se realizo la jugada ");
				    imprimirTablero();
			  }
			}
			if(juego.fin()==true)
			{
				System.out.println("fin del juego, el ganador es: "+juego.darAtacante());
				System.out.println("gracias por jugar");
				System.out.println("¿quiere jugar una nueva partida?");
				System.out.println("1. si");
				System.out.println("2. no");
				int s=Integer.parseInt(sc.next());
				if(s==1)
				{
					empezarJuegoMaquina();
				}
			}
		}
		}
		  //TODO
	}
	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		String[][] w=juego.darTablero();
		
		  System.out.println(w);
		}
		//TODO
}
